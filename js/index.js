/*
    Data persistance start
*/
//localStorage syntax and use case
localStorage.setItem("name", "Bob");

const savedName = localStorage.getItem("name");

console.log("name", savedName);

localStorage.removeItem("name");

//sessionStorage syntax and use case
//expires when user close the tab
sessionStorage.setItem("surname", "Bobitskiy");

const savedSurname = sessionStorage.getItem("surname");

console.log("savedSurname", savedSurname);

sessionStorage.removeItem("surname");

//Cookies syntax and use case
//Need server to interact
document.cookie = `father=Serhii; expires=${new Date(
  2024,
  1,
  3,
  17,
  54,
  30
).toUTCString()}`;

document.cookie = `mother=Alina; expires=${new Date(
  2024,
  1,
  3,
  17,
  54,
  30
).toUTCString()}`;

const allStoredInCookies = document.cookie;

console.log("allStoredInCookies", allStoredInCookies);

const transformCookiesDataIntoObject = (cookiesData) => {
  return cookiesData
    .split(";")
    .map((data) => data.split("="))
    .reduce((resultObject, [key, value]) => {
      resultObject[key.trim()] = decodeURIComponent(value);
      return resultObject;
    }, {});
};

const transformedObject = transformCookiesDataIntoObject(allStoredInCookies);

console.log("transformedObject", transformedObject);
